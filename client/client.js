if (Meteor.isClient) {
    Meteor.subscribe('dragons');
    Template.Home.events({
        'click .card': function(e, t) {
            console.log(e.target);
        },
        'hover .card': function(e, t) {
            return console.log(e.target);
        }

    });
    Template.AddDragon.events({
        'submit #addDragon': function(e, t) {
            var image, name, price;
            e.preventDefault();
            name = e.target.name.value;
            price = e.target.price.value;
            image = e.target.image.value;
            link = e.target.link.value;
            if (!link) {
                link = 'https://www.facebook.com/DungeonBeardedDragonsAndOtherCreatures';
            }
            Meteor.call('addDragon', name, price, image, link);
            console.log('Added ' + name + 'for $' + price + ' with image of ' + image)
        }
    });
    Template.Home.helpers({
        dragons: function() {
            return Dragons.find().fetch();
        },
        total_dragons: function() {
            return Dragons.find().count();
        }
    });
    

    Template.AdminCards.helpers({
      dragons: function(){
        return Dragons.find().count();
      }
    });
    Template.AdminCards.events({
      'click .admin.card': function(e, t) {
        console.log('Removed');
        Meteor.call('removeDragon', this._id);
      }
    });
}
