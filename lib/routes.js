FlowRouter.route('/', {
    action: function(params) {
        FlowLayout.render('Home');
    }
});
FlowRouter.route('/admin', {
    action: function(params) {
        FlowLayout.render('Admin');
    }
});