if (Meteor.isServer) {
    // Accounts.validateNewUser(function() {
    //     return false;
    // });
    Meteor.publish('dragons', function() {
        return Dragons.find();
    });
    Dragons.allow({
        'insert': function(id, doc) {
            return true;
        }

    });
    Meteor.methods({
        addDragon: function(name, price, image, link) {
            Dragons.insert({
                name: name,
                price: price,
                image: image,
                link: link
            });
        },
        removeDragon: function(id) {
            Dragons.remove(id)
        }
    });
}